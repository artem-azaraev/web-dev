console.info('Loaded');

const vect = [{ 'n': 4 }, { 'n': 2 }, { 'n': 6 }];

// let sumBy = _.sumBy(vect, 'n');
// console.info('My external attempt', sumBy);

window.addEventListener('load', () => {
  console.info("Loaded complete");
  let sumBy = _.sumBy(vect, 'n');
  console.info('My attempt inside onload', sumBy);

});

console.info(document.querySelector('#navigation-page'));
document.querySelector('#navigation-page').addEventListener('click', function() {
  window.location = "2-navigation.html";
});
