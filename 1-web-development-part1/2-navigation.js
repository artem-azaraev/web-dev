const hash = window.location.hash.substr(1);
console.info(hash);

window.addEventListener('load', () => {
  document.querySelector('#start').addEventListener('click', function() {
    runTimerOnHistory();
  });

  document.querySelector('#get-count').addEventListener('click', function() {
    alert(window.location.href.replace('http://localhost:8080/2-navigation-', '').replace('.html', ''));
  });
});

function runTimerOnHistory() {
  let count = 1;
  const interval = setInterval(() => {
    window.history.pushState({}, '', `2-navigation-${count}.html`);
    count++;
    if(count > 20) {
      clearInterval(interval);
    }
  }, 1000);
}
