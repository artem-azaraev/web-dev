/**
 * Primitives
 */
var bool = true;

var nNull = null;
var nNull = Null;

var undef = undefined;
var undef;

var num = 3.76;

var bigI = 9007199254740992n;

var sString = 'My string';
var sString = "My string";

var symb1 = Symbol('Sym');
var symb2 = Symbol('Sym');

symb1 === symb2

/**
 * Objects and functions
 */
var obj = {};
var funk = function() {};
console.info(obj);
console.info(funk);

/**
 * == vs ===
 */
var bool = false;
var sString = '';
var num = 0;
var nNull = null;

bool == num;
bool == sString;
sString == num;
bool == sString;
bool == nNull;

bool === num;
