/**
 * Functions as classes, old school
 */
var Person = function(name) {
  // Private this
  var self = {};
  self.name = name;
  // Private method
  function getName() {
    return self.name;
  }
  // Public method
  this.sayHello = function() {
    return getName();
  };
};

var pers1 = new Person("John Doe");
pers1.sayHello();

/**
 * Static methods
 */
var Person = function(name) {
  // Private this
  var self = {};
  self.name = name;
  // Private method
  function getName() {
    return self.name;
  }
  // Public method
  this.sayHello = function() {
    return getName();
  };
};
Person.staticMethod = function() {
  return "I'm static";
};

var pers1 = new Person("John Doe");
pers1.sayHello();
Person.staticMethod();
pers1.staticMethod();
/**
 * Issues
 */
var Person = function(name) {
  // Private this
  var self = {};
  self.name = name;
  // Private method
  function getName() {
    return self.name;
  }
  // Public method
  this.sayHello = function() {
    return getName();
  };
};

var pers1 = new Person("John Doe");
var pers2 = new Person("Jane Doe");

pers1.sayHello === pers2.sayHello;

/**
 * Using prototype
 */
var Person = function(name) {
  // Private this
  var self = {};
  self.name = name;
  // Private method
  function getName() {
    return self.name;
  }
};

Person.prototype.sayHello = function() {
  return getName();
};

var pers1 = new Person("John Doe");
var pers2 = new Person("Jane Doe");

pers1.sayHello === pers2.sayHello;

pers1.sayHello();
