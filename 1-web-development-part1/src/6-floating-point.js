/**
 * Floating point issue
 */
var a = 0.1;
var b = 0.2;

console.info(0.1 * 0.2);


console.info(5.33 + 5.2);

/**
 * Read more on the subject
 * https://modernweb.com/what-every-javascript-developer-should-know-about-floating-points/
 */
