/**
 * This in script
 */
console.info(this);

var a = 'my value';

console.info(window.a);
console.info(window['a']);

/**
 * This in a function
 */
function funk() {
  console.info(this);
}

funk();

/**
 * This in a closure function
 */
function Person() {
  function sayHello() {
    console.info(this);
  }
  return sayHello;
}

sayHello();
Person();

/**
 * This in instance of a FUNCTION
 */
function Person() {
  console.info(this);
}

var pers1 = new Person();



