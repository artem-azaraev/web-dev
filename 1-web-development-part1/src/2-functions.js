/**
 * Functions, display functions console.info vs debugger
 */
function funk() {
  console.info('function funk()');
}

var funk = function () {
  console.info('var funk');
};

/**
 * Anonymous functions
 */
function() {
  console.info('funk');
};

(function() {
  console.info('funk');
})();

/**
 * Arrow functions
 */
var funk = () => {
  console.info('arrow funk')
};

/**
 * Calling function and parameters
 */
var funk = (a , b , c) => {
  console.info('arrow funk', a, b, c);
};
funk(1, 2, 3);

funk.call(this, 1, 2, 3);
funk.apply(this, [1, 2, 3]);

/**
 * Calling function arguments/ arrow functions
 */
console.info(arguments);
var funk = function(a, b, c) {
  console.info('arrow funk', arguments);
};
funk(1, 2, 3);

funk.call(this, 1, 2, 3);
funk.apply(this, [1, 2, 3]);

/**
 * Function hoisting
 */
foo(); // "bar"

function foo() {
  console.log('bar');
}

/**
 * Function expression
 */
baz();

var baz = function() {
  console.log('bar2');
};

/**
 * To be discussed in 2-functions debug vs console.info, look at the funk variable with debugger
 */
const obj = {};
const funk = function() {

};
console.info(funk);
