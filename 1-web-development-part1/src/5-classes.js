/**
 * Simple class
 */
class Person {
  constructor(name) {
    this.name = name;
  }
  sayHello() {
    return this.name;
  }
}
var pers1 = new Person("John Doe");
pers1.sayHello();

/**
 * Private fields
 */
class Person {
  #name;
  constructor(name) {
    this.#name = name;
    this.myname = name;
  }

  sayHello() {
    return this.#name;
  }

  sayHello2() {
    return "Hello my name is "+this.myname;
  }
}
var pers1 = new Person("Jane Doe");
console.info(pers1.name);
console.info(pers1.sayHello());
console.info(pers1.sayHello2());


/**
 * Private Method
 */
class Person {
  #name;
  constructor(name) {
    this.#name = name;
  }

  #sayHello() {
    return this.#name;
  }

  sayHello2() {
    return "Hello my name is "+this.#sayHello();
  }
}
var pers1 = new Person("Jane Doe");
console.info(pers1.name);
console.info(pers1.sayHello2());

/**
 * Static Method
 */
class Person {
  #name;
  constructor(name) {
    this.#name = name;
  }

  #sayHello() {
    return this.#name;
  }

  sayHello2() {
    return "Hello my name is "+this.#sayHello();
  }

  static myStaticMethod() {
    return "I'm static";
  }
}
var pers1 = new Person("Jane Doe");
console.info(pers1.name);
console.info(pers1.sayHello2());
console.info(pers1.myStaticMethod());
console.info(Person.myStaticMethod());
