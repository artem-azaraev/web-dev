import React from 'react';
import {MyClass} from "./MyClass";
import {TestMe} from "./components/TestMe";
import {Tab, Tabs} from "react-bootstrap";

function App() {
    let myInput = 'Hello again';
    const handleMyOutput = ()=> {
        myInput += '!!!';
    };
  return (
    <div className="App">
      Hello World
        <TestMe myInput={myInput} myOutput={handleMyOutput}/>
        <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example" className="mb-3">
            <Tab eventKey="home" title="Home">
                <TestMe myInput={myInput} myOutput={handleMyOutput}/>
            </Tab>
            <Tab eventKey="profile" title="Profile">
                <TestMe myInput={myInput} myOutput={handleMyOutput}/>
            </Tab>
            <Tab eventKey="contact" title="Contact" disabled>
                <TestMe myInput={myInput} myOutput={handleMyOutput}/>
            </Tab>
        </Tabs>
    </div>
  );
}

export default App;
