const template = document.createElement('template');
const columnTemplate = document.createElement('template');
const cellTemplate = document.createElement('template');

function getColumnTemplate(label) {
  columnTemplate.innerHTML = `
    <th>${label}</th>
  `;
  return columnTemplate;
}

function getCellTemplate(value) {
  cellTemplate.innerHTML = `
    <td>${value}</td>
  `;
  return cellTemplate;
}

template.innerHTML = `
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.css">
    <table class="table table-dark table-striped">
        <thead id="header">
            <tr></tr>
        </thead>
        <tbody id="body">
        </tbody>
    </table>
`;

class RGTable extends HTMLElement {
  #options;

  constructor(options = {}) {
    super();
    this.#options = options;
    this.attachShadow({mode: 'open'});
    this.shadowRoot.appendChild(template.content.cloneNode(true));

    const theadElement = this.shadowRoot.querySelector('#header tr');
    const tbodyElement = this.shadowRoot.querySelector('#body');

    this.#options.columns.forEach((column) => {
      const template = getColumnTemplate(column.label);
      theadElement.appendChild(template.content.cloneNode(true));
    });

    this.#options.data.forEach((row) => {
      const trElement = document.createElement('tr');
      this.#options.columns.forEach((column) => {
        const template = getCellTemplate(row[column.key]);
        trElement.appendChild(template.content.cloneNode(true));
      });
      tbodyElement.appendChild(trElement);
    });
  }
}

window.customElements.define('rg-table', RGTable);

export {RGTable};
