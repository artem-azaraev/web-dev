import {autocomplete} from './autocomplete/autocomplete.js';
import {WcAutocomplete} from './wc-autocomplete/wc-autocomplete.js';

window.addEventListener('load', () => {
  // autocomplete('#main-search');

  // const inputElement = document.createElement('input');
  // inputElement.setAttribute('type', 'text');
  // document.querySelector('#normal-js').appendChild(inputElement);
  // autocomplete(inputElement, {onItemClicked: (item) => alert(item.title)});

  const autocompleteElement = new WcAutocomplete();
  document.querySelector('#web-component').appendChild(autocompleteElement);
});
