function autocomplete(selector, options = {onItemClicked: console.info}) {
  const element = typeof selector !== 'string' ? selector : document.querySelector(selector);
  let resultListElement;

  const init = () => {
    element.addEventListener('keyup', async () => {
      query(element.value).then(buildResultList);
    });
    element.addEventListener('blur', hideResultList);
    element.addEventListener('focus', showResultList);
  };

  const buildContainer = () => {
    if(!resultListElement) {
      resultListElement = document.createElement('div');
      resultListElement.classList.add('autocomplete-results');

      element.parentElement.append(resultListElement);
    }
    hideResultList();
    return resultListElement;
  };
  const buildResultList = (data) => {
    buildContainer();
    showResultList();
    resultListElement.innerHTML = '';
    data.forEach((item) => {
      const itemEl = buildResultItem(item);
      itemEl.addEventListener('mousedown', () => handleItemClick(item));
      resultListElement.append(itemEl);
    });
  };
  const buildResultItem = (item) => {
    const itemEl = document.createElement('div');
    itemEl.classList.add('result-item');
    itemEl.append(item.title);
    const smallEl = document.createElement('small');
    smallEl.append(item.author);
    itemEl.append(smallEl);
    return itemEl;
  };

  const hideResultList = () => {
    if(!resultListElement) return;
    resultListElement.classList.remove('show');
    resultListElement.classList.add('hide');
  };
  const showResultList = () => {
    if(!resultListElement) return;
    resultListElement.classList.add('show');
    resultListElement.classList.remove('hide');
  };

  const handleItemClick = (item) => {
    options.onItemClicked(item);
  };

  const query = async (value) => {
    const response = await fetch(`http://localhost:3000/libraries?q=${value}`);

    return await response.json();
  };

  init();
}

export {autocomplete};
